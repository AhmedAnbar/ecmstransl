<?php

namespace Ecms\Translate;

use Ecms\Translate\Commands\DBmigrate;
use Ecms\Translate\Commands\Initialization;
use Illuminate\Support\ServiceProvider;

class TranslateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Initialization::class,
                DBmigrate::class,
            ]);
        }
        
    }

    /**
     * Bootstrap services.
     *
     * @return void 
     */
    public function boot()
    {
        // Load Routes from route file
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        // Load Config file
        $this->mergeConfigFrom(__DIR__ . '/config/ecmstranslate.php', 'ecmstranslate');
        // Publish config file name "ecmstranslate.php"
        $this->publishes([__DIR__.'/config/ecmstranslate.php' => config_path('ecmstranslate.php'),]);
        // Load translatoins file
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'translate');
        // Publish translations files
        $this->publishes([__DIR__.'/resources/lang' => resource_path('lang/'),]);
        // Load Views files
        $this->loadViewsFrom(__DIR__.'/resources/views', 'ecmstranslate');
        // Publish views files
        $this->publishes([__DIR__.'/resources/views' => resource_path('views/admin/system/translation'),]);
        // Publish dependency assets
        $this->publishes([__DIR__.'/public/admin-themes/' => public_path('admin-themes'),], 'public');
    }
}
