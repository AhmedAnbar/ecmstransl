<?php

namespace Ecms\Translate\Models;

use Ecms\Translate\Events\RecordCreated;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
	
	protected $dispatchesEvents = [
		'created' => RecordCreated::class,
		'updated' => RecordCreated::class,
		'deleted' => RecordCreated::class,
	];
	
}


