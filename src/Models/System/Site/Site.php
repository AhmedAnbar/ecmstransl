<?php

namespace Ecms\Translate\Models\System\Site;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model {
	
	use SoftDeletes;
	
	protected static function boot() {
		parent::boot();
		
		
		static::addGlobalScope('module', function(Builder $builder) {
			$builder->where(function($query) {
				$query->whereNull('domain')->orWhere('domain', env('APP_DOMAIN'));
			})->where(function($query) {
					$query->whereNull('record_state')->orWhere('record_state', 1);
				});
		});
		
		// Order by name ASC
		static::addGlobalScope('order', function(Builder $builder) {
			$builder->orderBy('record_priority', 'asc')
				->orderBy('site_title', 'asc');
		});
	}
}
