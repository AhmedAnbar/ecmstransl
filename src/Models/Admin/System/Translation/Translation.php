<?php

namespace Ecms\Translate\Models\Admin\System\Translation;

use Ecms\Translate\Models\BaseModel;
use Ecms\Translate\Models\Triggers\CreatedBy;
use Ecms\Translate\Models\Triggers\UpdatedBy;
use Illuminate\Database\Eloquent\SoftDeletes;

class Translation extends BaseModel {
	
	use SoftDeletes, CreatedBy, UpdatedBy;
	
	protected $fillable = [
		'site_id',
		'site_lang',
		'full_path',
		'parent_id',
		'translation_key',
		'translation',
		'translations_raw',
		'translation_published',
	];
	
}


