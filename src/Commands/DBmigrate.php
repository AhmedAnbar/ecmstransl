<?php

namespace Ecms\Translate\Commands;

use Ecms\Translate\Commands\Helpers\SqlMigrations;
use Illuminate\Console\Command;

class DBmigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ecms:DB-Migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert SQL database to migration files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Type database name: ";
        $handle = fopen ("php://stdin","r");
        $database_name = trim(fgets($handle));
        if ($database_name === null || $database_name == '') {
            echo "\nYou must type database name.\n";
            return;
        }
        echo "Type list of tables that you want to ignore \"e.g. users, password_resets\": ";

        $handleIgnors = fopen ("php://stdin","r");
        $tablesArr = explode(',', fgets($handleIgnors));
        $ignoringTables = array();
        if(count($tablesArr) > 0) {
            foreach ($tablesArr as $key => $table) {
                array_push($ignoringTables, trim($table));
            }
        }
        echo "\nStarting ...\n";
        $migrate = new SqlMigrations();
        $migrate->ignore($ignoringTables);
        $migrate->convert($database_name);
        echo "\nScanning ...\n";
        $migrate->write();
        echo "\nDone.\n";
    }
}
