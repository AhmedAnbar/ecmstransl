<?php

namespace Ecms\Translate\Commands;

use Ecms\Translate\Http\Controllers\TranslationController;
use Illuminate\Console\Command;

class Initialization extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ecms:Translations-Init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize Translations table in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $translations = new TranslationController();
        echo "Initializing...\n";
        if($translations->initiate('123123')){
            echo "Done...\n";
        } else {
            echo "Error!!\n";
        }

    }
}
