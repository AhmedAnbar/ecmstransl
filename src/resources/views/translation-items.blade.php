
<form class="kt-form" enctype="multipart/form-data" method="POST" id="MainForm" name="MainForm">
    @csrf
    <input type="text" hidden name="parent_id" id="parent_id" value="{{@$translationParentId}}">

    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr class="thead-light">
                <th class="text-center">{{__('admin.system.translations.Key')}}</th>
                @foreach(@$Sites as $site)
                    <th class="text-center">
                        {{$site->site_title}}
                    </th>
                @endforeach
            </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            @if($item->full_path != null)
            <tr class='' id="tr_{{$item->id}}">
                <td class="text-truncate">
                    {{$item->translation_key}}
                    <br/><small>{{$item->full_path}}</small>
                </td>
                @foreach($Sites as $site)
                    <td class="text-center">
                        <div class="input-group">
                            <input dir="{{$site->site_direction}}" id="{{$site->site_lang}}_{{$item->id}}" name="{{$site->site_lang}}_{{$item->id}}" onfocusout="updateTranslation(this,'{{$item->id}}','{{$site->site_lang}}')" value="{{@json_decode(@$item->translations,true)[$site->site_lang]}}" type="text" class="form-control">
                            @if($site->id <> 1)
                                <button type="button" onclick="googleTranslation('{{$site->site_lang}}_{{$item->id}}','{{$item->id}}','{{$site->site_lang}}','{{$item->translation_key}}')" class="btn btn-label-brand btn-bold">
                                    <i class="fas fa-language"></i>
                                </button>
                            @endif
                        </div>
                    </td>
                @endforeach
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>

</form>
