<?php
//############################################################################
// ADMIN PANEL
//############################################################################
Route::group(['prefix' => 'admin', 'namespace' => 'Ecms\Translate\Http\Controllers',], function() {
				
  Route::group(['prefix' => 'translation',], function() {
    
    Route::get('translations-gui', function() {
      return 'show gui';
    })->name('translations.gui-show');

    Route::get('translations', 'TranslationController@index')->name('translations.index');
    Route::get('jsonTranslation', 'TranslationController@jsonTranslation')->name('jsonTranslation');
    
    Route::get('translations-search', 'TranslationController@search')->name('translations.search');
    
    Route::get('translations-item/{id?}', 'TranslationController@show')->name('translations-item');
    
    Route::post('translations-update', 'TranslationController@update')->name('translations.update');
    Route::get('translations-scan', 'TranslationController@scanTranslation')->name('translations.scan');
    Route::get('translations-publish', 'TranslationController@publishTranslation')->name('translations.publish');
    Route::get('translations-unpublished', 'TranslationController@unpublishedTranslation')->name('translations.unpublished');
    Route::get('translations-create', 'TranslationController@newTranslation')->name('translations.create');
    
    Route::get('initiate/{code?}', 'TranslationController@initiate')->name('initiate');
    
    
  });
		
});