<?php

return [
    //Methods to scan to find all translations used in the application
    'scan_methods' => [
        'trans',
        'trans_choice',
        'Lang::get',
        'Lang::choice',
        'Lang::trans',
        'Lang::transChoice',
        '@lang',
        '@choice',
        '__'
    ],
    //Paths to scan to find unpublished translations
    'scan_paths' => [
        'views',
//        'Helpers',
//        'Http/Controllers',
//        'Models',
//        'Resources',
//        'Rules'
    ]
];