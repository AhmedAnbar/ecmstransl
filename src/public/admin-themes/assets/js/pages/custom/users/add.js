// Class definition
var KTUsersAdd = function () {
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	var avatar;

	// Private functions
	var initWizard = function () {
		var avatar = new KTAvatar('kt_user_avatar');

		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v3', {
			startStep: 1,
		});

		// Change event
		wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {

	}

	var initSubmit = function () {

	}

	return {
		// public functions
		init: function () {
			wizardEl = KTUtil.get('kt_wizard_v3');
			formEl = $('#kt_form');

			initWizard();
			initValidation();
			//initSubmit();
		}
	};
}();

jQuery(document).ready(function () {
	KTUsersAdd.init();
});