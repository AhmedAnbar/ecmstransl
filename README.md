## Installation

Install the package via Composer

`composer require ecmstest/laravel-translate`

Publish configuration

`php artisan vendor:publish --provider="Ecms\Translate\TranslateServiceProvider"`

## Required
+ Jquery js
+ Jquery Form js  
+ Bootstrap

## Usage
### Initialization
+ Edit database in _**.env**_ file
+ edit below lines to  _**.env**_ file
```
APP_DEFAULT_LANGUAGE=ar
APP_DOMAIN=translate.test
```
+ Migrate table to the database by type
```
php artisan migrate
```
+ run initialize command by type (if this is your first use)
```
php artisan Ecms:Translations-Init
```
### Migrations -- (todo)
Migrations files published in _**database/migrations**_ folder

### Configuration

The package ships with a configuration file called `ecmstranslate.php` which is published to the
config directory during installation. Below is an outline of the settings.

```
scan_methods [array]
```
Choose which laravel translation method to use when searching for translations.

```
scan_paths [array]
```
Choose which paths to use when searching for missing translations. Narrowing the
search to specific directories will result in a performance increase when
scanning for missing translations. 

### Views -- (bug)
Main layout should locate in 
```
resources/views/admin [folder]
```
include in the head of main layout 
```
@yield('pageCsCode')
@yield('pageJsCode')
```
in the body 
```
@yield('pageContent')
```
and in the footer
```
@yield('pageJsCode2')
```

### Assets
Assets published in _**admin-theme**_ folder in public directory

### Language
Language files published to _**resources/lang**_ folder 
### Test
run laravel server
```
php artisan serve
```
in the browser go to
```
http://127.0.0.1:8000/admin/translation/translations
```